# Chart


## BarChart

![barchart](image/BARCHART.png )

| Prop | Type | Description |
| --- | ----------- | ----------- |
| type | Object |  barChart  |
| title | String |  |
| data | Array | |
| --- | In Data Array | ---|
| seriesName | String |  |
| color | String | color code  |
| data | Array | {x: "BM", y: 2}  |


## Example Usage

```json

[
    {
        "data":[
            {
                "type":"barChart",
                "title":"",
                "data":[
                    {
                        "seriesName":"BM",
                        "color":"#007fff",
                        "data":[
                            {
                                "x":"BM",
                                "y":2,
                            },
                            {
                                "x":"BM",
                                "y":4,
                            },
                        ]
                    },
                    {
                        "seriesName":"BI",
                        "color":"#f4c2c2",
                        "data":[
                            {
                                "x":"BI",
                                "y":5,
                            },
                            {
                                "x":"BI",
                                "y":4,
                            },
                        ]
                    },
                    {
                        "seriesName":"BCC",
                        "color":"#fe6f5e",
                        "data":[
                            {
                                "x":"BCC",
                                "y":1,
                            },
                            {
                                "x":"BCC",
                                "y":4,
                            },
                        ]
                    }
                ]
            }
        ]
    }
]

```

---


## lineChart

![line](image/LINECHART.png )

| Prop | Type | Description |
| --- | ----------- | ----------- |
| type | Object |  lineChart  |
| title | String |  |
| data | Array |  |
| --- | In Data Array | ---|
| seriesName | String |  |
| color | String | color code  |
| data | Array | {x: "BM", y: 2}  |


## Example Usage

```json

[
    {
        "data":[
            {
            "type":"lineChart",
            "title":"",
            "data":[
                {
                    "seriesName": "series1",
                    "data": [
                        {"x": "2018-02-01", "y": 30},
                        {"x": "2018-02-02", "y": 200},
                        {"x": "2018-02-03", "y": 170},
                        {"x": "2018-02-04", "y": 250},
                        {"x": "2018-02-05", "y": 10}
                    ],
                    "color": "#297AB1"
                },
                {
                    "seriesName": "series2",
                    "data": [
                        {"x": "2018-02-01", "y": 20},
                        {"x": "2018-02-02", "y": 100},
                        {"x": "2018-02-03", "y": 140},
                        {"x": "2018-02-04", "y": 550},
                        {"x": "2018-02-05", "y": 40}
                    ],
                    "color": "yellow"
                }
            ]
        }
    ]
    }
]

```

---


## PieChart

![Pie](image/PIECHART.png )

| Prop | Type | Description |
| --- | ----------- | ----------- |
| type | Object |  pieChart  |
| title | String |  |
| data | Array |  |
| color | Array |  |
| --- | In Data Array | ---|
| x | String | label/ name |
| color | String | color code  |
| y | Number |   |
| label_2 | String | category name  |



## Example Usage

```json

[
    {
        "data":[
            {
                "type":"pieChart",
                "title":"借书种类",
                "data":[
                    {x: "111", y: 111, color: "#007fff", label_2: "Bahasa"}
                    {x: "75", y: 75, color: "#f4c2c2", label_2: "Seni dan Rekreasi"}
                    {x: "31", y: 31, color: "#fe6f5e", label_2: "Sains"}
                    {x: "20", y: 20, color: "#79443b", label_2: "1"}
                    {x: "9", y: 9, color: "#de3163", label_2: "Agama"}
                ],
                "color":[
                    "#007fff", 
                    "#f4c2c2", 
                    "#fe6f5e",
                    "#79443b"
                    "#de3163"
                ]
            }
        ]
    }
]

```

---

## DonutChart

![Donut](image/DONUTCHART.png )

| Prop | Type | Description |
| --- | ----------- | ----------- |
| type | Object |  donutChart  |
| title | String |  |
| data | Array |  |
| color | Array |  |
| --- | In Data Array | ---|
| x | String | label/ name |
| color | String | color code  |
| y | Number |   |
| label_2 | String | category name  |



## Example Usage

```json

[
    {
        "data":[
            {
                "type":"donutChart",
                "title":"大多数读者借阅的书籍",
                "data":[
                    {x: "64", y: 64, color: "#007fff", label_2: "THE LAST FROG PRINCE"}
                    {x: "25", y: 25, color: "#f4c2c2", label_2: "秘密花园"}
                    {x: "23", y: 23, color: "#fe6f5e", label_2: "Mysterious Light Source"}
                    {x: "21", y: 21, color: "#79443b", label_2: "RAJA SINGA YANG BIJAK"}
                    {x: "20", y: 20, color: "#de3163", label_2: "人体历险记：孙悟空科学72变"}
                ],
                "color":[
                    "#007fff", 
                    "#f4c2c2", 
                    "#fe6f5e",
                    "#79443b"
                    "#de3163"
                ]
            }
        ]
    }
]

```

---

## Radar Chart

![radar](image/RADARCHART.png )

| Prop | Type | Description |
| --- | ----------- | ----------- |
| type | Object |  radarChart  |
| title | String |  |
| data | Array | Title of next screen |
| --- | In Data Array | ---|
| seriesName | String |  |
| color | String | color code  |
| data | Array | {x: "BM", y: 2}  |


## Example Usage

```json

[
    {
        "data":[
            {
                "type":"radarChart",
                "title":"",
                "data":[
                    {
                        seriesName: 'series1',
                        data: [
                            {x: '2018-02-01', y: 30},
                            {x: '2018-02-02', y: 200},
                            {x: '2018-02-03', y: 170},
                            {x: '2018-02-04', y: 250},
                            {x: '2018-02-05', y: 10}
                        ],
                        color: '#297AB1'
                    },
                    {
                        seriesName: 'series2',
                        data: [
                        {x: '2018-02-01', y: 20},
                        {x: '2018-02-02', y: 100},
                        {x: '2018-02-03', y: 140},
                        {x: '2018-02-04', y: 550},
                        {x: '2018-02-05', y: 40}
                        ],
                        color: 'yellow'
                    }
                ],
                "code":[
                    "BCC": "BAHASA CINA SJK (C)",
                    "BI": "BAHASA INGGERIS",
                    "BM": "BAHASA MELAYU"
                ]
            }
        ]
    }
]

```

---
