# Demo

![productui](image/ProductUI-1.png )
![demo2](image/demo2.png )
![demo3](image/demo3.png )


# Product_Header


| Prop | Type | Description |
| --- | ----------- | ----------- |
| type |  |  product_header  |
| display | Array |  |

![productheader](image/ProductUI.png )

## Example Usage

```json
{
    "type":"product_header",
    "display":[
        "数学华语",
        "Author: Hui"
    ]
}

```

---

# Product_Credit


| Prop | Type | Description |
| --- | ----------- | ----------- |
| type |  |  product_credit  |
| display | Array |  |

![productcredit](image/ProductCredit.png )

## Example Usage

```json
{
    "type":"product_credit",
    "display":[
        "RM54.90",
        "92% off - 2 days left at this price"
    ]
}

```

---



# Rating_Star


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | rating_star                            |         String |
display       | Initial value for the rating           |         Number |
css           |        |         Object  |
configuration |      |         Object  |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
selectedColor        | Pass in a custom fill-color for the rating icon                     |         String |
alignSelf            | ('auto', 'flex-start', 'flex-end', 'center', 'stretch', 'baseline') |         String | null
marginLeft           |                                                                     |         String | null
size                 | Size of the rating star                                             |         Number | 30
labelFontSize                 |                                              |         Number | 26
marginRight                 |                                              |         Number | 


## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
count        | Total number of ratings to display                                                                                         |         Number  |
disabled   | Whether the rating can be modiefied by the user                                                                            |         Boolean |
showLabel    | true -> Will use "display" value as Text and appear beside the stars                                                       |         Boolean |
showRating   | Determines if to show the reviews above the rating                                                                         |         Boolean |
reviews      | Labels to show when each value is tapped e.g. If the first star is tapped, then value in index 0 will be used as the label |         Array   |
url          |                                                                                                                            |         String  |


![ratingstar](image/RatingStar.png )

---

![ratingstar2](image/RatingStar2.png )

## Example Usage

```json

{
    "type":"rating_star",
    "css":{
        "selectedColor":"grey",
        "alignSelf":"flex-start",
        "marginLeft":30,
        "size":15,
        "labelFontSize":18,
        "marginRight":30,
    },
    "configuration":{
        "count":5,
        "disabled":true,
        "showLabel":true,
        "showRating":false,
        "reviews":[],
        "url":""
    },
    "display":4.6,    
}

```



---




# Information_List



## Configuration Array
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type        | information_list |         Number  |
headerLabel       |                  |         String  |
display     |                  |         Array   |

![information](image/InformationList.png )


## Example Usage

```json

{
    "type":"information_list",
    "headerLabel":"Information",
    "display":{
        "Difficulty": "Normal",
        "Credit": "300",
        "Subject": "数学",
        "Level": "Level Six",
        "Author": "Hui",
        "Total Question": 20
    }
}


```

---





# Ratings_Reviews


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | ratings_reviews                            |         String |
display       | Initial value for the rating           |         Number |
css           |        |         Object  |
configuration |      |         Object  |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
barColor        |      |         String | null
fillColor            | background color of bar    |         String | null
btnColor           |         |         String | null


## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
displayTitle        |  text to display as title                          |         String  |
totalRating         |  how many user had rate this item       |         String |
ratingDetails      | total amount of each Star,   [ '5star', '4star', '3star', '2star', '1star' ]  |         Array with Number |
btnLabel   |       label that beside title (able to click)        |         String | null 
url          |     if url = "" , btn_label will be ignore  |         String  |
title          |   title of next screen                      |         String  |


![ratingsreviews](image/RatingsReviews.png )


## Example Usage

```json

{
    "type":"ratings_reviews",
    "css":{
        "barColor":"#2150D0",
        "fillColor":"#dfe6e9",
        "btnColor":"blue"
    },
    "configuration":{
        "url":"",
        "title":"feedbackscreen",
        "displayTitle":"Ratings & Reviews",
        "totalRating":"938 Ratings",
        "btnLabel":"See All",
        "ratingDetails":[
            0.8,
            0,
            0.1,
            0.1,
            0.2
        ]
    },
    "display":4.4
}

```


---





# Feedbacks

If user click on write reviews (feedback_submited is false) , a modal will pop out

Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | feedbacks                            |         String |
display       |  label will display while feedbackSubmitted is false         |         String |
css           |        |         Object   |
configuration |      |         Object  |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
iconSize        |      |         Number | 22
fontSize            | background color of bar    |         Number | 18


## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
feedbackSubmitted        |  if feedbackSubmitted is true, "display" & "icon" will be ignore                         |         Boolean  |
icon         |  icon beside label       |        Fontawesome  |
modalDetails   |            |         Object |  
data          |    reviews data   |         Array  |


## Modal_Details Array

Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
modalTitle        |                         |         String  |
modalCancel         |  label of cancel button       |        String  |
modalSubmit   |      label of submit button      |         String |  
modalInputPlaceholder          |   label to let user know that what shall they need to tap    |         String  |
modalInputName         |  name that you will need to request from api       |        String  |
modalInputDefault   |            |         String |  
requestData   |      to let me get all this data & key and pass back to server      |         Object |  


## Request_Data Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
url        |                   |         String  |
title         |  title of next screen       |        String  |
params   |     all the key that you will need to request      |         Array |  


![feedbacks](image/Feedbacks.png )


## Example Usage

```json

{
    "type":"feedbacks",
    "css":{
        "iconSize":22,
        "fontSize":18,
    },
    "configuration":{
        "feedbackSubmitted": false,
        "icon":"edit",
        "modalDetails":{
            "modalTitle":"Write a Review",
            "modalCancel":"Cancel",
            "modalSubmit":"Send",
            "modalInputPlaceholder":"title",
            "modalInputName":"review_title",
            "modalInputDefault":"review_content",
            "requestData":{
                "url":"",
                "title":"",
                "params": [
                    "token",
                    "review_title",
                    "review_content",
                ]
            }
        },
       
    },
    "display":"Write a Review"
}

```


---



# ProductIntro


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | product_intro                            |         String |
display       |           |         String |
css           |        |         Object   |
configuration |      |         Object  |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
backgroundColor        |      |         String | 
marginLeft            |     |         Number | 
marginTop            |     |         Number | 
marginBottom            |     |         Number | 
borderWidth            |     |         Number | 
marginRight            |     |         Number | 
center            |     |         Boolean | 
fontSize            |     |         Number |
fontColor            |     |         String | 
fontFamily            |     |         String | 
fontWeight            |     |         String | 


## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
headerLabel        |                           |         String  |
html         |       |        Boolean  |

## Example Usage

```json

{
    "type":"product_intro",
    "css":{
        "iconSize":22,
        "fontSize":18
    },
    "configuration":{
        "headerLabel": "",
        "html":true
    },
    "display":""
}

```


---



# Tags


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | tags                            |         String |
display       |           |         Array |
css           |        |         Object   |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
backgroundColor        |      |         String | 
marginLeft            |     |         Number | 
marginTop            |     |         Number | 
marginBottom            |     |         Number | 
borderWidth            |     |         Number | 
marginRight            |     |         Number | 


## Example Usage

```json

{
    "type":"tags",
    "css":{
        
    },
    "display":[]
}

```



---



# Button


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | button                            |         String |
display       |           |         Array |
css           |        |         Object   |
configuration           |        |         Object   |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
textColor        |      |         String | 
buttonColor            |     |         Number | 
margin            |     |         Number | 



## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
url        |                           |         String  |
title         |       |        String  |
disabled         |       |        Boolean |


## Example Usage

```json

{
    "type":"button",
    "css":{
        "textColor":"",
        "buttonColor":"",
        "margin":""
    },
    "configuration":{
        "url":"",
        "title":"",
        "disabled":false
    },
    "display":[]
}

```



---



# FeedbacksList


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | feedbacks_list                            |         String |
display       |           |         Array |
css           |        |         Object   |
configuration           |        |         Object   |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
ratingColor        |      |         String | 



## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
feedbackRating        |                           |         String  |
feedbackTitle         |       |        String  |
feedbackDate         |       |        String |


## Example Usage

```json

{
    "type":"feedbacks_list",
    "css":{
        "ratingColor":"",
    },
    "configuration":{
        "feedbackTitle":"Interactive, Fun and interesting way ofasdasdasdasd",
        "feedbackRating":5,
        "feedbackDate":"Wed",
        "feedbackName":"Jchang99"
    },
    "display":[
        "feedbackContent":"React Native JS code runs as a web worker inside this tab.\n Press ⌘⌥I to open Developer Tools. Enable Pause On Caught Exceptions for a better debugging experience.\nYou may also install the standalone version of React Developer Tools to inspect the React component hierarchy, their props, and state.\nStatus: Debugger session #0 active.",
    ]
}

```




---



# Tags


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | tags                            |         String |
display       |           |         Array |
css           |        |         Object   |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
borderWidth        |      |         Number | 
backgroundColor        |      |         String | 


## Example Usage

```json

{
    "type":"tags",
    "css":{
        "borderWidth":0,
        "backgroundColor":"",
    },
    "display":[
        "Tag 1",
        "Tag 2",
        "Tag 3"
    ]
}

```