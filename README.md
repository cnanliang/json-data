# Fab

A floating action button represents the primary action in an application.

![fab](image/FAB.png )


props                     | type                            | description           | default
:------------------------ |:-------------------------------------- | --------------:|:------------------
fab                    |                            |         key | 
url            | String        |         API of next screen | null
title               | String   |         Title of next screen | null


## Example Usage

```json

[
    {
        "fab": {
        "url": "http://school.edudios.com/sis/api2/home/44D3F7154CF819607BBC8DDB13D2C924",
        "title": "AddStudent"
        },
        "data":[]
    }
]

```

---

# Web View

WebView renders web content in a app view.

![web_view](image/WEBVIEW.png )

| --- | ----------- | ----------- | 
| Prop | Type | Description | 
| --- | ----------- | ----------- | 
| web_view | String |  API  |


## Example Usage

```json

[
    {
        "web_view":"http://9a7d1c68.ngrok.io/general/api/main/CB4FCB1EF8C6828AC2C6FCA7DE3A312C/",
        "data":[] 
    }
]


```

---

# Topic

 User  subscribe these topics to receive specific notification.
 
 | --- | ----------- | ----------- | 
 | Prop | Type | Description | 
 | --- | ----------- | ----------- | 
 | topic | Array |  topics  |
 
 
## Example Usage
 
```json
 
 [
     {
         "topic": [
         "CE88E709C239DD98C9BAB8B9DCF355AA"
         ],
         "data":[] 
     }
 ]
 
 
```
 
 ---

# Background Color

Set background color for current screen.
 
 | --- | ----------- | ----------- | 
 | Prop | Type | Description | 
 | --- | ----------- | ----------- | 
 | background_color | String |  color code |
 
 
## Example Usage
 
```json
 
 [
     {
         "background_color":"#23233",
         "data":[] 
     }
 ]
 
 
```
 
 ---
# Below component need to set inside  Data Array
---

# Toolbar 

 
 Toolbar is usually used as a header placed at the top of the screen. It can contain the screen title, controls such as navigation buttons, menu button etc.
  
  ![toolbar](image/TOOLBAR.png )

| --- | ----------- | ----------- | 
 | Prop | Type | Require | Description | 
 | --- | ----------- | ----------- | 
 | type | String | true  |  toolbar |
 | title | String | true |Text for the title. && Title of next screen |
 | subtitle | String | false | Text for the subtitle. |
 | icon | Fontawesome5 / IconSource | false  |  default type is IconSource, you will need to request if when you need to use other icon source  |
 | credit | Number |  false  | Number need to show on right of the header  |
 | url | String | false | API of next screen  |
 
 
## Example Usage
 
```json
 
 [
     {
         "background_color":"#23233",
         "data":[
             {
                 "type": "toolbar",
                 "title": "SCORE 100",
                 "subtitle": "",
                 "icon": "coins",
                 "credit": 2500,
                 "url": "http://0b4bab30.ngrok.io/general/api/main/849F8F330BCA75D75F152998D22ADFFB"
             },
         ] 
     }
 ]
 
 
```
 
 ---
 
 
# Text 
 
 ![text](image/TEXT.png )
 
 | --- | ----------- | ----------- | 
 | Prop | Type | Require | Description | 
 | --- | ----------- | ----------- | 
 | type | String | true  |  text |
 | status | String | false | to aviod error screen, we can set status = false |
 | center | boolean | false  |  textalign center  |
 | title | string |  false  | this font style will be set as bold  |
 | data | Array/ String |  false  | if center = false then data type = string  |
 
 
## Example Usage
 
```json
 
 [
     {
         "background_color":"#23233",
         "data":[
             {
                 "type": "text",
                 "status": false,
                 "center": false,
                 "title": "Something Went Wrong",
                 "data": "Please try again later"
             },
         ] 
     }
 ]
 
 
```
 
---

# QrCode Scanner 

![qrscanner](image/QRSCANNER.png )

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  qrCodeScanner |


## Example Usage

```json

[
    {
        "background_color":"#23233",
        "data":[
            {
                "type": "qrCodeScanner",
            },
        ] 
    }
]


```

---


# Camera 

![camera](image/CAMERA.png )

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  camera |
| icon_color | String | true  |  camera |
| camera_icon | Fontawesome | true  |  icon |


## Example Usage

```json

[
    {
    "background_color":"#23233",
    "data":[
        {
            "type": "camera",
            "icon_color":"white",
            "camera_icon":"camera"
        },
    ] 
    }
]


```

---


# Grid View 

![grid_view](image/GRIDVIEW.png )
![grid_view1](image/GRIDVIEW1.png )

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  grid_view |
| data | Array | true  |  all below props will be inside this data array |
| --- | ----------- | ----------- | 
| icon | Fontawesome | false  |  -  |
| name | String | true  | label of the each item |
| iconcolor | String | false  |  color code |
| url | String | true  |  api of next screen |
| title | String | true  |  title of next screen |


## Example Usage

```json

[
    {
        "data":[
            {
                "type": "grid_view",
                "data": [
                    {
                        "icon": "users",
                        "name": "Parent",
                        "iconcolor": "black",
                        "url": "",
                        "title": "StudentList",
                    },
                    {
                        "icon": "user-tie",
                        "name": "Principal",
                        "iconcolor": "black",
                        "url": "",
                        "title": "Principal",
                    },
                ]
            }
        ]
    }
]


```

---


# Separate Line 

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  separate_line |


## Example Usage


```json

[
    {
        "data": [
            {
            "type": "separate_line"
            },
        ]
    }
]


```

---


# Tab View

![multi_list_img](image/TABVIEW.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  tab_view |
| first | Array | true  |  Data Array for first page |
| second | Array | true  |  Data Array for second page |
| routes | Array | true  |   |



## Example Usage

```json

[
    {
        "data": [
           {
               "type": "tab_view",
               "first": [],
               "second": [],
               "routes": [
                   {
                       "key": "first",
                       "title": "All"
                   },
                   {
                       "key": "second",
                       "title": "Purchased"
                   }    
               ]
           }
        ]
    }
]


```

---


# Img

![img](image/IMG.png)
![img](image/IMG2.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  img |
| local | Boolean | true  |  image from local or URL link |
| img | String | false  |  when image = 1  |
| img_per_row | number | true  | default 1  |
| imageWidth | number | true  |   |
| imageHeight | number | true  |    |
| data | Array | false  |  when image > 1  |




## Example Usage

```json

[
    {
        "data": [
            {
                "type": "img",
                "local": true,
                "img_per_row":2,
                "imageWidth":150,
                "imageHeight":150,
                "data":["Challengers","Challengers"]
            },
        ]
    }
]


```

---


# Multi List 

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |


## img  ( multi_list )


![multi_list_img](image/MULTILIST-IMG.png)


| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |
| ------------------ | In Data Array | ------------------| 
| genre | String | true  |  img  |
| show_label | boolean | true  |   |
| clickable | boolean | false  |  click function |
| img_height | boolean | true  | true =  img width same with height , false img width < img height |
| remove | boolean | true  |  able to swipe to remove |
| add | boolean | true  |  a fab + button will be show on right side of the list when add = true |
| url | String | false  |  api of next screen  |
| title | String | false  |  titile of next screen |
| img | String | true  |  img source url  |
| data | Array | true  | data that need to list out   |




## Example Usage

```json

[
    {
        "data": [
            {
                "type": "multi_list",
                "data": {
                    "img_height":true,
                    "genre": "img",
                    "qr_url": "",
                    "show_label":true,
                    "clickable": false,
                    "url": "",
                    "title": "",
                    "remove": "",
                    "add": "",
                    "img":"",
                    "data": {
                        "Name": "JMY",
                        "empty_label_123":"阿斯顿",
                        "IC":"980301-08-9291"
                    }
                }
            },
        ]
    }
]


```

## icon  ( multi_list )

![mutli_list_icon](image/MULTILIST-ICON.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |
| ------------------ | In Data Array | ------------------ | 
| genre | String | true  |  icon  |
| show_label | boolean | true  |   |
| clickable | boolean | false  |  click function |
| url | String | false  |  api of next screen  |
| title | String | false  |  titile of next screen |
| icon | Fontawesome | true  |  available icon only from react-native-vector-icon directory |
| data | Array | true  | data that need to list out   |




## Example Usage

```json

[
    {
        "data": [
            {
                "type": "multi_list",
                "data": {
                    "genre": "icon",
                    "show_label":false,
                    "clickable": false,
                    "url": "",
                    "title": "",
                    "remove": "",
                    "add": "",
                    "icon_size":50,
                    "icon":"user",
                    "icon_color": "black",
                    "data": {
                        "icon": "User"
                    }
                }
            },
        ]
    }
    
]


```


## duo_img  ( multi_list )

![mutli_list_duo_img](image/MULTILIST-DUO-IMG.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |
| ------------------ | In Data Array | ------------------ | 
| genre | String | true  |  duo_img  |
| clickable | boolean | true  |    |
| show_label | boolean | false  |  |
| img_height | boolean | false  |  |
| url | String | false  |  api of next screen  |
| title | String | false  |  titile of next screen |
| img | String | true  | ImageSource URL  |
| target_img | String | true  | imageSource URL  |
| remove | String | true  |    |
| add | String | true  |    |
| data | Array | true  | data that need to list out   |




## Example Usage

```json

[
    {
        "data": [
            {
                "type": "multi_list",
                "data": {
                    "genre": "duo_img",
                    "add": "",
                    "show_label": false,
                    "clickable": true,
                    "img_height": true,
                    "remove": "",
                    "url": "http://5c372ea1.ngrok.io/general/api/main/20240E5431AA82B6F19D0181D0F77555/?challenge_ref=44C19A925F571B01D1AFAF59D5D70553&user_paper=E90BF28CE4395EF2AFB91B383EB8C20D",
                    "title": "challengeInfo",
                    "img": "https://graph.facebook.com/2576755475710645/picture?width=480&height=480",
                    "target_img": "https://graph.facebook.com/10213710066487943/picture?width=480&height=480",
                    "data": {
                        "middle_img": "VS"
                    }
                }
            },
        ]
    }
]


```


## thumbnail  ( multi_list )

![mutli_list_thumbnail](image/MULTILIST-THUMBNAIL.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |
| ------------------ | In Data Array | ------------------ | 
| genre | String | true  |  thumbnail  |
| clickable | boolean | true  |    |
| show_label | boolean | false  |  |
| img_height | boolean | false  |  |
| url | String | false  |  api of next screen  |
| title | String | false  |  titile of next screen |
| img | String | true  |  Left side Image  |
| img2 | String | true  |  Right side Image |
| label | String | true  |  Left side label  |
| label2 | String | true  |  Right side label |
| remove | String | true  |    |
| add | String | true  |    |




## Example Usage

```json

[
    {
        "data": [
            {
                "type": "multi_list",
                "data": {
                    "genre": "thumbnail",
                    "add": "",
                    "show_label": false,
                    "clickable": true,
                    "img_height": true,
                    "remove": "",
                    "url": "http://5c372ea1.ngrok.io/general/api/main/20240E5431AA82B6F19D0181D0F77555/?challenge_ref=44C19A925F571B01D1AFAF59D5D70553&user_paper=E90BF28CE4395EF2AFB91B383EB8C20D",
                    "title": "challengeInfo",
                    "font_color": "blue",
                    "label": "2019-September-05",
                    "label2": "Pending",
                    "img": "Date",
                    "img2": "Status",
                }
            },
        ]
    }
]


```


## duo_text  ( multi_list )

![mutli_list_duo_text](image/MULTILIST-DUO-TEXT.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |
| ------------------ | In Data Array | ------------------ | 
| genre | String | true  |  duo_text  |
| clickable | boolean | true  |    |
| show_label | boolean | false  |  |
| img_height | boolean | false  |  |
| url | String | false  |  api of next screen  |
| title | String | false  |  titile of next screen |
| text_left | String | true  |  Left side Image  |
| text_right | String | true  |  Right side Image |
| font_color | String | true  | color code |
| data | Array | true  | list_one and list_two required  |
| remove | String | true  |    |
| add | String | true  |    |




## Example Usage

```json

[
    {
        "data": [
            {
                "type": "multi_list",
                "data": {
                    "genre": "duo_text",
                    "add": "",
                    "show_label": false,
                    "clickable": true,
                    "img_height": true,
                    "remove": "",
                    "text_left": "Hui",
                    "text_right": "Jeremmy",
                    "font_color": "#2150D0",
                    "data": {
                        "list_one": {
                            "Name": "Hui"
                        },
                        "list_two": {
                            "Name": "Jeremmy"
                        }
                    }
                },
            }
        ]
    }
]


```


## duo_text  ( multi_list )

![mutli_list_duo_text](image/MULTILIST-DUO-TEXT.png)

| --- | ----------- | ----------- | 
| Prop | Type | Require | Description | 
| --- | ----------- | ----------- | 
| type | String | true  |  multi_list |
| data | Array | true  |   |
| ------------------ | In Data Array | ------------------ | 
| genre | String | true  |  duo_text  |
| clickable | boolean | true  |    |
| show_label | boolean | false  |  |
| img_height | boolean | false  |  |
| url | String | false  |  api of next screen  |
| title | String | false  |  titile of next screen |
| text_left | String | true  |  Left side Image  |
| text_right | String | true  |  Right side Image |
| font_color | String | true  | color code |
| data | Array | true  | list_one and list_two required  |
| remove | String | true  |    |
| add | String | true  |    |




## Example Usage

```json

[
{
"data": [
{
"type": "multi_list",
"data": {
"genre": "duo_text",
"add": "",
"show_label": false,
"clickable": true,
"img_height": true,
"remove": "",
"text_left": "Hui",
"text_right": "Jeremmy",
"font_color": "#2150D0",
"data": {
"list_one": {
"Name": "Hui"
},
"list_two": {
"Name": "Jeremmy"
}
}
},
}
]
}
]


```



---









**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
